# README #
This creates a template spec in the DevelopmentSupport Resource Group.

That spec can then be referenced by name and version when building new infrastructure.

# EXAMPLE #

```code

  $templateSpecId = (Get-AzTemplateSpec -Name clientSqlDatabse-master).Id

  New-AzResourceGroupDeployment -ResourceGroup '[client]-[env]' `
  -Name SqlDatabase `
  -TemplateSpecId $templateSpecId

```
